# Info

This repo only serves as a testing fixture for the forgejo-release-fetcher plugin.

Files:

`a-file-without-anything.txt`: Plain text file without any signatures

`just-a-test-file.txt` a medium text file, with matching sha256 and asc (signed with the private key matching the `public.key`)

`just-a-messed-with-test-file.txt` a copy of `just-a-test-file.txt` that was signed and checksummed and then modified, so sha and asc don't match

`public.key` the public key matching the private key used to sign, here for future reference.

The key used to sign is available on keys.openpgp.org as `F131CAD0B600E17C0D4C45F7F1798B7E890ADC67`
